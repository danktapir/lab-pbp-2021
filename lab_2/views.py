from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from lab_2.models import Note

# Create your views here.
def index_html(request):
    notes = Note.objects.all().values()
    context = {'notes': notes}
    return render(request, 'lab2.html', context)

def index_xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def index_json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
