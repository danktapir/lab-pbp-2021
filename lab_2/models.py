from django.db import models

# Create your models here.
class Note(models.Model):
    to_note = models.CharField(max_length=30)
    from_note = models.CharField(max_length=30)
    title_note = models.CharField(max_length=30)
    message_note = models.CharField(max_length=30)