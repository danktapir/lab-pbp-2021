from django.shortcuts import render
from lab_2.models import Note

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    context = {'notes': notes}
    return render(request, 'lab4_index.html', context)