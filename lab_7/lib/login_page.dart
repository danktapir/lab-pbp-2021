import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.blue, Colors.purple])),
          child: Column(
            children: [
              addCard(Icons.home, "Home Page"),
              addCard(Icons.shopping_cart_rounded, "Shopping Cart Page"),
              addCard(Icons.add_circle_rounded, "Wishlist Page"),
              addCard(Icons.masks_rounded, "Product List Page"),
              addCard(Icons.brush_rounded, "Customize Page")
            ],
          ),
        ),
      ),
      appBar: AppBar(
          title: Text("Login Page"),
          actions: [Icon(Icons.adb_rounded)],
          elevation: 5,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.blue, Colors.purple])),
          )),
      body: Form(
        key: _formKey,
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height / 2,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
                border: Border.all(),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey,
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3))
                ]),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Username must not empty!";
                      }
                      return null;
                    },
                    maxLength: 15,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.account_box_rounded),
                        labelText: "Username",
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        hintText: "Enter your username",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Password must not empty!";
                      }
                      return null;
                    },
                    obscureText: true,
                    maxLength: 20,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.vpn_key_rounded),
                        labelText: "Password",
                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        hintText: "Enter your password",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2,
                  child: ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Row(
                          children: [
                            Icon(Icons.info_outline_rounded,
                                size: 30, color: Colors.white),
                            Spacer(
                              flex: 1,
                            ),
                            Text("Login successful",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20))
                          ],
                        )));
                      }
                    },
                    child: Text(
                      "LOGIN",
                      style: TextStyle(letterSpacing: 2),
                    ),
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10))),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(top: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Don't have an account? Register "),
                        Text("here",
                            style: TextStyle(
                                color: Colors.blue[900],
                                decoration: TextDecoration.underline,
                                decorationStyle: TextDecorationStyle.solid,
                                decorationColor: Colors.blue[900])),
                      ],
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Card addCard(IconData iconData, String text) {
    return Card(
      elevation: 10,
      child: Row(
        children: [Icon(iconData), Text(text)],
      ),
    );
  }
}
