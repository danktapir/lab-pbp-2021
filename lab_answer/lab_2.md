1. Perbedaan antara JSON dan XML :
JSON : Mendukung penggunaan array, sintaks berupa string key/value pair, sintaks pada JSON relatif lebih pendek, dsb.
XML : Tidak mendukung penggunaan array, sintaks berupa tag (open and closed tag), sintaks pada XML lebih panjang, dsb.

2. Perbedaan antara HTML dan XML :
HTML : Bersifat case-insensitive, difokuskan pada penyajian data, beberapa tag HTML tidak mengharuskan untuk memakai closing tag, dsb.
XML : Bersifat case-sensitive, difokuskan pada transfer data, semua tag XML harus memiliki closing tag, dsb.


Referensi : https://blogs.masterweb.com/perbedaan-xml-dan-html/