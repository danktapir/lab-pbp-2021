import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.blue, Colors.purple])),
          child: ListView(
            children: [
              addCard(Icons.home, "Home Page"),
              addCard(Icons.shopping_cart_rounded, "Shopping Cart Page"),
              addCard(Icons.add_circle_rounded, "Wishlist Page"),
              addCard(Icons.masks_rounded, "Product List Page"),
              addCard(Icons.brush_rounded, "Customize Page")
            ],
          ),
        ),
      ),
      appBar: AppBar(
          title: Text("Login Page"),
          actions: [Icon(Icons.adb_rounded)],
          elevation: 5,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.blue, Colors.purple])),
          )),
      body: Center(
        child: Container(
          height: MediaQuery.of(context).size.height / 2,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
              border: Border.all(),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey,
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3))
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: TextField(
                  maxLength: 15,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.account_box_rounded),
                      labelText: "Username",
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      hintText: "Enter your username",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20))),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: TextField(
                  obscureText: true,
                  maxLength: 20,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.vpn_key_rounded),
                      labelText: "Password",
                      floatingLabelBehavior: FloatingLabelBehavior.always,
                      hintText: "Enter your password",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20))),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: ElevatedButton(
                  onPressed: () {},
                  child: Text(
                    "LOGIN",
                    style: TextStyle(letterSpacing: 2),
                  ),
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Text("Don't have an account yet? Register here"))
            ],
          ),
        ),
      ),
    );
  }

  Card addCard(IconData iconData, String text) {
    return Card(
      child: Row(
        children: [Icon(iconData), Text(text)],
      ),
    );
  }
}